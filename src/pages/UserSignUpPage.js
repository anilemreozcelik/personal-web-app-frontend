import React from 'react'
import { signup } from '../api/apiCalls';
import Input from '../components/input'
class UserSignUpPage extends React.Component {

    state={
        username: null,
        displayname: null,
        password: null,
        repeatpassword: null,
        pendingApiCall:false,
        errors: {}
    };


        onChange=event => {
            const {name,value} = event.target; //object destructring
            const errors = {...this.state.errors}
            errors[name] = undefined
            if(name=== 'password' || name==='passwordRepeat'){
                if(name==='password' && value !==this.state.passwordRepeat){
                    errors.passwordRepeat='Password mismatch';
                }else if(name==='passwordRepeat' && value!==this.state.password){
                    errors.passwordRepeat='Password mismatch';
                }else{
                    errors.passwordRepeat=undefined;    
                }
            }
            this.setState({
                [name] : value,
                errors
            })
        }

        onClickSignUp = async event => {
            event.preventDefault();
            const {username,displayname,password} = this.state;

            const body ={
                username,
                displayname,
                password
            };
            this.setState({pendingApiCall:true})

            try{
                const response= await signup(body);
            }catch(error){
                console.log(error.response.data.message);
                if(error.response.data.validationErrors){
                this.setState({errors:error.response.data.validationErrors})
                }
            }
            this.setState({pendingApiCall:false});
        }

    render(){
        const {pendingApiCall,errors} = this.state;
        const {username,displayname,password,passwordRepeat} = errors;

        return(
            <div className="container">
                <form>
                <h1 className="text-center">SignUp</h1>
                <Input name="username" label="Username" error={username} onChange={this.onChange} />
                <Input name="displayname" label="Display Name" error={displayname} onChange={this.onChange} />
                <Input name="password" label="Password" error={password} onChange={this.onChange} type="password"/>
                <Input name="passwordRepeat" label="Password Repeat" error={passwordRepeat} onChange={this.onChange} type="password"/>

                <div className="text-center">
                <button className="btn btn-primary" 
                onClick={this.onClickSignUp}
                disabled={pendingApiCall || passwordRepeat!==undefined}
                >
                    {pendingApiCall && <span className="spinner-border spinner-border-sm"></span>}
                    Sign Up</button>
                </div>
            </form>
            </div>
        );
    }


}

export default UserSignUpPage;